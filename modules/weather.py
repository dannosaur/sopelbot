# coding=utf-8
from datetime import datetime

import pytz
import requests
from sopel.config import StaticSection
from sopel.config.types import ValidatedAttribute
from sopel.module import commands


class WeatherSection(StaticSection):
    darksky_api_key = ValidatedAttribute('darksky_api_key', str)


def configure(config):
    config.define_section('weather', WeatherSection)
    config.admin.configure_setting('darksky_api_key', "DarkSky API key")


def setup(bot):
    bot.config.define_section('weather', WeatherSection)


@commands('setweather')
def setweather(bot, trigger):
    location = trigger.group(2)

    if not location:
        coords = bot.db.get_nick_value(trigger.nick, 'weather_coords')
        if not coords:
            bot.say("You don't have a location set currently.")
            return

        latitude, longitude = coords.split(',')
        bot.say(f"Your current stored location: {latitude}, {longitude}")
        return

    coords = geocode_location(trigger.group(2))

    if coords:
        bot.db.set_nick_value(trigger.nick, 'weather_coords', ','.join([coords['latitude'], coords['longitude']]))
        bot.db.set_nick_value(trigger.nick, 'weather_location_name', coords['address'])
        bot.say("Stored new location")
    else:
        bot.say(f"No co-ordinates found for '{location}'")


@commands('weather', 'w')
def getweather(bot, trigger):
    extra = trigger.group(2)

    if not extra:
        coords = bot.db.get_nick_value(trigger.nick, 'weather_coords')
        if not coords:
            bot.say("You don't have a location set currently.")
            return

        latitude, longitude = coords.split(',')

        address = bot.db.get_nick_value(trigger.nick, 'weather_location_name')
    else:
        if extra.startswith('@'):
            nick = extra[1:]
            coords = bot.db.get_nick_value(nick, 'weather_coords')
            if not coords:
                bot.say(f"{nick} doesn't have a location set currently.")
                return

            latitude, longitude = coords.split(',')
            address = bot.db.get_nick_value(nick, 'weather_location_name')
        else:
            coords = geocode_location(extra)
            if not coords:
                bot.say("I couldn't find that location.")
                return

            latitude = coords['latitude']
            longitude = coords['longitude']
            address = coords['address']

    api_key = bot.config.weather.darksky_api_key
    coord_str = ','.join([latitude, longitude])

    response = requests.get(
        url=f'https://api.darksky.net/forecast/{api_key}/{coord_str}',
        params={
            'language': 'en',
            'units': 'us',
            'exclude': 'minutely,hourly,daily,alerts,flags',
        }
    )
    data = response.json()

    currently = data['currently']
    tz = pytz.timezone(data.get('timezone') or 'UTC')

    updated = datetime.fromtimestamp(currently['time']).astimezone(tz)
    temperature = currently['temperature']  # in f
    apparent_temperature = currently['apparentTemperature']  # in f
    dewpoint = currently['dewPoint']
    condition = currently['summary']
    pressure = currently['pressure']
    wind_bearing = currently['windBearing']
    wind_speed = currently['windSpeed']
    wind_gust = currently['windGust']
    visibility = currently['visibility']
    uv_index = currently['uvIndex']
    humidity = int(currently['humidity'] * 100)

    # Weather info for <Location> (updated on 9:31 AM EDT on March 26, 2019):
    # Temperature: 44.8°F / 7.1°C
    # Windchill: 43°F / 6°C
    # Humidity: 99%
    # Dew Point: 44°F / 7°C
    # Wind: SSW at 4 mph / 6.4 km/h
    # Wind Gust: 5.0 mph / 8.0 km/h
    # Pressure: 30.49 in / 1032 hPa (Rising)
    # Conditions: Clear
    # Visibility: 10.0 miles / 16.1 kilometers
    # UV: 3 out of 16

    weather_info = f"Weather info for {address} " \
        f"(updated {updated.strftime('%H:%M on %B %d, %Y %Z')}): " \
        f"{condition} | " \
        f"Temperature: {f_to_c(temperature)}°C / {round(temperature)}°F | " \
        f"Feels like: {f_to_c(apparent_temperature)}°C / {round(apparent_temperature)}°F | " \
        f"Dewpoint: {f_to_c(dewpoint)}°C / {round(dewpoint)}°F | " \
        f"Humidity: {humidity}% | " \
        f"Wind: {bearing_to_compass(wind_bearing)} at {wind_speed}mph / {miles_to_km(wind_speed)}km/h, gusting to {wind_gust}mph / {miles_to_km(wind_gust)}km/h | " \
        f"Pressure: {hpa_to_in(pressure)} in / {pressure} hPa | " \
        f"Visibility: {visibility} miles / {miles_to_km(visibility)} km | " \
        f"UV index: {uv_index}"
    bot.say(weather_info)


def f_to_c(f_value):
    return round((f_value - 32) * (5/9))


def km_to_miles(kmh_value):
    return round(kmh_value / 1.609)


def miles_to_km(mph_value):
    return round(mph_value * 1.609)


def hpa_to_in(hpa_value):
    return round(hpa_value / 33.863886666667, 2)


def bearing_to_compass(bearing_value):
    if 0 <= bearing_value <= 11.25:
        return "N"
    elif 11.25 < bearing_value <= 33.75:
        return "NNE"
    elif 33.75 < bearing_value <= 56.25:
        return "NE"
    elif 56.25 < bearing_value <= 78.75:
        return "ENE"
    elif 78.75 < bearing_value <= 101.25:
        return "E"
    elif 101.25 < bearing_value <= 123.75:
        return "ESE"
    elif 123.75 < bearing_value <= 146.25:
        return "SE"
    elif 146.25 < bearing_value <= 168.25:
        return "SSE"
    elif 168.25 < bearing_value <= 191.25:
        return "S"
    elif 191.25 < bearing_value <= 213.75:
        return "SSW"
    elif 213.75 < bearing_value <= 236.25:
        return "SW"
    elif 236.25 < bearing_value <= 258.75:
        return "WSW"
    elif 258.75 < bearing_value <= 281.25:
        return "W"
    elif 281.25 < bearing_value <= 303.75:
        return "WNW"
    elif 303.75 < bearing_value <= 326.25:
        return "NW"
    elif 326.25 < bearing_value <= 348.75:
        return "NNW"
    elif 348.75 < bearing_value <= 360:
        return "N"


def geocode_location(location):
    response = requests.get(
        url='https://www.geocode.farm/v3/json/forward/',
        params={
            'addr': location
        },
        verify=False,
    )
    data = response.json()

    geocoding_results = data.get('geocoding_results', {})
    results = geocoding_results.get('RESULTS', [])

    if len(results) > 0:
        result = results[0]

        return {
            'latitude': result['COORDINATES']['latitude'],
            'longitude': result['COORDINATES']['longitude'],
            'address': result['formatted_address'],
            'address_data': result['ADDRESS'],
        }
