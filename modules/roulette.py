# -*- coding: utf-8 -*-
import random

from sopel.module import commands
from sopel.tools import SopelMemory


def setup(bot):
    if 'roulette_state' not in bot.memory:
        bot.memory['roulette_state'] = SopelMemory()

    reset_roulette(bot, announce=False)


@commands('roulette')
def do_roulette(bot, trigger):
    roulette_state = bot.memory['roulette_state']

    if roulette_state['last_player'] == trigger.nick:
        bot.say("You can't play twice in a row. You got a death wish or something?!")
        return

    roulette_state['last_player'] = trigger.nick
    if roulette_state['current_chamber'] == roulette_state['chamber_number']:
        bot.say(f"** BANG ** {trigger.nick} is dead. RIP")
        reset_roulette(bot)
    else:
        if roulette_state['current_chamber'] == 5:
            bot.say(f"Click!! {trigger.nick} brushed with death and won the game! [{roulette_state['current_chamber']}/6]")
            reset_roulette(bot)
        else:
            bot.say(f"Click... {trigger.nick} got lucky this time! [{roulette_state['current_chamber']}/6]")
            roulette_state['current_chamber'] += 1


def reset_roulette(bot, announce=True):
    if announce:
        bot.action("reloads the revolver...")

    roulette_state = bot.memory['roulette_state']
    roulette_state['current_chamber'] = 1
    roulette_state['chamber_number'] = random.choice(range(1, 7))
    roulette_state['last_player'] = None
