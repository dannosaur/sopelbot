import json
import socket
import struct
import time

import requests
from sopel.config import StaticSection
from sopel.config.types import ValidatedAttribute
from sopel.formatting import CONTROL_BOLD
from sopel.module import commands


class MinecraftSection(StaticSection):
    host = ValidatedAttribute('host', str)
    port = ValidatedAttribute('port', int)
    glances_port = ValidatedAttribute('glances_port', int)
    server_arch = ValidatedAttribute('server_arch', str)
    glances_net_if_name = ValidatedAttribute('glances_net_if_name', str)


def setup(bot):
    bot.config.define_section('minecraft', MinecraftSection)


def configure(config):
    config.define_section('minecraft', MinecraftSection)
    config.admin.configure_setting('host', "Server host/IP")
    config.admin.configure_setting('port', "Server port")
    config.admin.configure_setting('glances_port', "glances server port")
    config.admin.configure_setting('server_arch', "Server architecture (windows/linux)")
    config.admin.configure_setting('glances_net_if_name', "Network interface name on host")


@commands('mc')
def get_minecraft_data(bot, trigger):
    host = bot.config.minecraft.host
    port = bot.config.minecraft.port

    slp = StatusPing(host=host, port=port)
    status = slp.get_status()

    bot.say(f"[Minecraft v{status['version']['name']} @ {host}:{port}] {status['description']['text']}")

    online = ''
    if status['players']['online'] > 0:
        online = '- '
        online += ', '.join(sample['name'] for sample in status['players']['sample'])

    bot.say(f"{status['players']['online']}/{status['players']['max']} players online {online}")


@commands('mcstats')
def get_minecraft_server_stats(bot, trigger):
    host = bot.config.minecraft.host
    port = bot.config.minecraft.glances_port
    arch = bot.config.minecraft.server_arch
    glances_net_if_name = bot.config.minecraft.glances_net_if_name

    base_url = f'http://{host}:{port}/api/3'
    base_headers = {
        'Accept-Encoding': 'None'
    }

    net_url = f'{base_url}/network'
    mem_url = f'{base_url}/mem'

    if arch == 'windows':
        proc_url = f'{base_url}/processlist/name/java.exe'
    elif arch == 'linux':
        proc_url = f'{base_url}/processlist/name/java'
    else:
        raise Exception(f"Unknown server arch {arch}")

    net_response = requests.get(url=net_url, headers=base_headers)
    net_data = net_response.json()
    interface_data = list(filter(lambda i: i['interface_name'] == glances_net_if_name, net_data))[0]
    int_rx = round(interface_data['rx'] / 8000, 1)
    int_tx = round(interface_data['tx'] / 8000, 1)

    mem_response = requests.get(url=mem_url, headers=base_headers)
    mem_data = mem_response.json()
    mem_total = round(mem_data['total'] / 1073741824, 1)

    proc_response = requests.get(url=proc_url, headers=base_headers)
    proc_data = proc_response.json()

    if arch == 'windows':
        java = proc_data['java.exe'][0]
    elif arch == 'linux':
        java = proc_data['java'][0]
    else:
        # exception thrown above, this is just to stop pycharm whining
        return

    cpu_usage = java['cpu_percent']
    ram_pct = round(java['memory_percent'], 1)
    ram_usage = round(mem_total * (ram_pct / 100), 2)

    bot.say(f"[Minecraft] {CONTROL_BOLD}Server stats{CONTROL_BOLD} -- "
            f"{CONTROL_BOLD}CPU{CONTROL_BOLD} {cpu_usage}% | "
            f"{CONTROL_BOLD}RAM{CONTROL_BOLD} {ram_usage}GB ({ram_pct}% of {mem_total}GB) | "
            f"{CONTROL_BOLD}Net{CONTROL_BOLD} rx: {int_rx}KB/s / tx: {int_tx}KB/s")


# blatantly copy/pasted from https://gist.github.com/MarshalX/40861e1d02cbbc6f23acd3eced9db1a0
class StatusPing:
    """ Get the ping status for the Minecraft server """

    def __init__(self, host='localhost', port=25565, timeout=5):
        """ Init the hostname and the port """
        self._host = host
        self._port = port
        self._timeout = timeout

    def _unpack_varint(self, sock):
        """ Unpack the varint """
        data = 0
        for i in range(5):
            ordinal = sock.recv(1)

            if len(ordinal) == 0:
                break

            byte = ord(ordinal)
            data |= (byte & 0x7F) << 7 * i

            if not byte & 0x80:
                break

        return data

    def _pack_varint(self, data):
        """ Pack the var int """
        ordinal = b''

        while True:
            byte = data & 0x7F
            data >>= 7
            ordinal += struct.pack('B', byte | (0x80 if data > 0 else 0))

            if data == 0:
                break

        return ordinal

    def _pack_data(self, data):
        """ Page the data """
        if type(data) is str:
            data = data.encode('utf8')
            return self._pack_varint(len(data)) + data
        elif type(data) is int:
            return struct.pack('H', data)
        elif type(data) is float:
            return struct.pack('Q', int(data))
        else:
            return data

    def _send_data(self, connection, *args):
        """ Send the data on the connection """
        data = b''

        for arg in args:
            data += self._pack_data(arg)

        connection.send(self._pack_varint(len(data)) + data)

    def _read_fully(self, connection, extra_varint=False):
        """ Read the connection and return the bytes """
        packet_length = self._unpack_varint(connection)
        packet_id = self._unpack_varint(connection)
        byte = b''

        if extra_varint:
            # Packet contained netty header offset for this
            if packet_id > packet_length:
                self._unpack_varint(connection)

            extra_length = self._unpack_varint(connection)

            while len(byte) < extra_length:
                byte += connection.recv(extra_length)

        else:
            byte = connection.recv(packet_length)

        return byte

    def get_status(self):
        """ Get the status response """
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as connection:
            connection.settimeout(self._timeout)
            connection.connect((self._host, self._port))

            # Send handshake + status request
            self._send_data(connection, b'\x00\x00', self._host, self._port, b'\x01')
            self._send_data(connection, b'\x00')

            # Read response, offset for string length
            data = self._read_fully(connection, extra_varint=True)

            # Send and read unix time
            self._send_data(connection, b'\x01', time.time() * 1000)
            unix = self._read_fully(connection)

        # Load json and return
        response = json.loads(data.decode('utf8'))
        response['ping'] = int(time.time() * 1000) - struct.unpack('Q', unix)[0]

        return response
