# -*- coding: utf-8 -*-
import datetime
import json
import os
import random
import re
from pprint import pprint

import dateutil.parser
import twython
from sopel.config import StaticSection
from sopel.config.types import ValidatedAttribute
from sopel.formatting import bold
from sopel.module import rule, commands, interval
from sopel.tools import SopelMemory

tw = None
tweet_regex = r'.*(https://twitter\.com/(.*)/status/(\d+)).*'
tweet_pattern = re.compile(tweet_regex)


class TwitterSection(StaticSection):
    consumer_key = ValidatedAttribute('consumer_key', str)
    consumer_secret = ValidatedAttribute('consumer_secret', str)
    access_key = ValidatedAttribute('access_key', str)
    access_secret = ValidatedAttribute('access_secret', str)


def setup(bot):
    global tw

    bot.config.define_section('twitter', TwitterSection)

    if not bot.memory.contains('url_callbacks'):
        bot.memory['url_callbacks'] = SopelMemory()
    bot.memory['url_callbacks'][tweet_pattern] = get_tweet

    tw = twython.Twython(
        bot.config.twitter.consumer_key,
        bot.config.twitter.consumer_secret,
        bot.config.twitter.access_key,
        bot.config.twitter.access_secret,
    )

    load_state(bot)


def configure(config):
    config.define_section('twitter', TwitterSection)
    config.admin.configure_setting('consumer_key', "Consumer key")
    config.admin.configure_setting('consumer_secret', "Consumer secret")
    config.admin.configure_setting('access_key', "Access key")
    config.admin.configure_setting('access_secret', "Access secret")


def shutdown(bot):
    del bot.memory['url_callbacks'][tweet_pattern]


@rule(tweet_regex)
def get_tweet(bot, trigger):
    tweet_id = trigger.group(3)
    show_tweet(bot, tweet_id)


@interval(120)
def get_followers(bot):
    global tw

    following = bot.memory['twitter']['following']
    for user in following.keys():
        last_tweet_id = following[user]['last_tweet_id']
        channels = following[user]['channels']
        tweets = tw.get_user_timeline(
            screen_name=user,
            since_id=last_tweet_id,
            include_rts=False,
        )

        if len(tweets) > 0:
            _tweets = sorted(tweets, key=lambda t: t['id'], reverse=False)
            for tweet in _tweets:
                _tweet = tw.show_status(id=tweet['id'], tweet_mode='extended')
                for channel in channels:
                    bot.say(recipient=channel, text=render_tweet(_tweet, with_url=True))

                if tweet['id'] > last_tweet_id:
                    last_tweet_id = tweet['id']

        following[user]['last_tweet_id'] = last_tweet_id

    save_state(bot)


def show_tweet(bot, tweet_id, with_url=False):
    global tw

    try:
        tweet = tw.show_status(id=tweet_id, tweet_mode='extended')
    except:
        bot.say("Sorry, I couldn't find a tweet with that ID")
        return

    if tweet['user']['screen_name'].lower() == 'realdonaldtrump':
        strings = [
            "You made me waste an API call on that loser?",
            "BURN THE DEVIL!",
            "What is this guy even talking about?!",
            "I will not inflict that guy's nonsense upon this channel. Thank you, and goodbye.",
            "Go read this account's tweets instead: https://twitter.com/realDonaldTrfan",
            f"[Twitter] [{datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S %Z')}] <@realDonaldTrump> Blah blah blah Something Racist blah blah WALL! COVFEFE! HAMBERDERS!! [RT {random.randint(0, 999)} | <3 0]"
        ]
        bot.say(random.choice(strings))
        return

    bot.say(render_tweet(tweet, with_url=with_url))


def render_tweet(tweet, with_url=False):
    tweet_text = tweet['full_text']
    tweet_text = tweet_text.replace("\n", " ")

    if 'quoted_status_permalink' in tweet:
        tweet_text = tweet_text.replace(tweet['quoted_status_permalink']['url'], '')

    created = dateutil.parser.parse(tweet['created_at'])
    posted_date = created.strftime('%Y-%m-%d %H:%M:%S %Z')

    extra = ""
    if with_url:
        extra = f" | View on Twitter: https://twitter.com/{tweet['user']['screen_name']}/status/{tweet['id']}"

    return f"[Twitter] [{posted_date}] <@{tweet['user']['screen_name']}> {tweet_text} [RT {tweet['retweet_count']} | <3 {tweet['favorite_count']}]{extra}"


@commands('twitter', 'tw')
def do_twitter_cmd(bot, trigger):
    parts = trigger.group(2).split(' ')

    if re.match(r'\d+', parts[0]):
        show_tweet(bot, parts[0])

    elif parts[0] == 'show':
        show_tweet(bot, parts[1])

    elif parts[0] == 'follow':
        screen_name = parts[1].lower()

        if screen_name == 'realdonaldtrump':
            bot.say("Nope, not doing it...")
            return

        following = bot.memory['twitter'].get('following', {})

        if screen_name in following and trigger.sender in following[screen_name]['channels']:
            bot.say(f"I'm already following {screen_name}")
            return

        try:
            tweets = tw.get_user_timeline(screen_name=screen_name, count=1)
        except:
            bot.say(f"[Twitter] Error loading tweets for {screen_name}.")
            return

        if len(tweets) == 0:
            bot.say(f"[Twitter] Sorry, this user hasn't tweeted anything yet.")
            return

        last_tweet = tweets[0]

        if screen_name in following:
            following[screen_name]['channels'].append(trigger.sender)
            following[screen_name]['channels'] = list(set(following[screen_name]['channels']))
        else:
            following[screen_name] = {
                'channels': [trigger.sender],
                'last_tweet_id': last_tweet['id'],
            }

        bot.memory['twitter']['following'] = following
        save_state(bot)

        bot.say(f"[Twitter] Now following @{screen_name}. New tweets will show up when posted from this user.")

    elif parts[0] == 'unfollow':
        screen_name = parts[1].lower()

        following = bot.memory['twitter'].get('following', {})

        if screen_name not in following.keys():
            bot.say(f"I'm not following {screen_name}")
            return

        if len(following[screen_name]['channels']) > 1:
            following[screen_name]['channels'].remove(trigger.sender)
        else:
            del following[screen_name]

        bot.memory['twitter']['following'] = following
        save_state(bot)

        bot.say(f"[Twitter] I'm no longer following {screen_name}")

    elif parts[0] == 'search':
        do_search(bot, parts[1])

    elif parts[0] == 'ratelimit' and trigger.owner:
        result = tw.request(
            endpoint='https://api.twitter.com/1.1/application/rate_limit_status.json',
        )

        bot.say(bold("Twitter rate limit status"))

        resources = result['resources']

        statuses = resources['statuses']['/statuses/show/:id']
        statuses_reset = datetime.datetime.fromtimestamp(statuses['reset']).strftime('%Y-%m-%d %H:%M:%S %Z')
        statuses_used = statuses['limit'] - statuses['remaining']
        statuses_pct = int(round((statuses_used / statuses['limit']) * 100, 0))
        bot.say(f"- Show tweet: {statuses['remaining']}/{statuses['limit']} ({statuses_pct}%) - Reset: {statuses_reset}")

        timeline = resources['statuses']['/statuses/user_timeline']
        timeline_reset = datetime.datetime.fromtimestamp(timeline['reset']).strftime('%Y-%m-%d %H:%M:%S %Z')
        timeline_used = timeline['limit'] - timeline['remaining']
        timeline_pct = int(round((timeline_used / timeline['limit']) * 100, 0))
        bot.say(f"- User timeline: {timeline['remaining']}/{timeline['limit']} ({timeline_pct}%) - Reset: {timeline_reset}")

        search = resources['search']['/search/tweets']
        search_reset = datetime.datetime.fromtimestamp(search['reset']).strftime('%Y-%m-%d %H:%M:%S %Z')
        search_used = search['limit'] - search['remaining']
        search_pct = int(round((search_used / search['limit']) * 100, 0))
        bot.say(f"- Search: {search['remaining']}/{search['limit']} ({search_pct}%) - Reset: {search_reset}")


def do_search(bot, q):
    results = tw.search(q=q, count=15, result_type='recent', include_entities=False)
    tweets = list(results['statuses'])

    tweet = random.choice(tweets)
    show_tweet(bot, tweet['id'], with_url=True)


def save_state(bot):
    with open(os.path.join(bot.config.core.homedir, 'twitter.json'), 'w') as fh:
        fh.write(json.dumps(bot.memory['twitter']))


def load_state(bot):
    conf_path = os.path.join(bot.config.core.homedir, 'twitter.json')
    if not os.path.exists(conf_path):
        bot.memory['twitter'] = {}
        return

    with open(conf_path, 'r') as fh:
        bot.memory['twitter'] = json.loads(fh.read())


# search shortcuts
@commands('deranged')
def do_search_derangeddonald(bot, trigger):
    do_search(bot, '#DerangedDonald')
